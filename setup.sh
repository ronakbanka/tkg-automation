#!/bin/bash
set -e

TKG_BUCKET="tkg-mgmt-bucket-${CI_PROJECT_NAME}-${CI_PROJECT_ID}"
TKG_CONFIG="$HOME/.tkg/config.yaml"
TKG_KEYPAIR="tkg-mgmt-key-${CI_PROJECT_NAME}-${CI_PROJECT_ID}"
TKG_MGMT_CLUSTER="tkg-mgmt-cluster-${CI_PROJECT_NAME}-${CI_PROJECT_ID}"
KUBECONFIG="$HOME/.kube/config"
export AWS_REGION=$AWS_DEFAULT_REGION

function download_cli {
    
    echo "Installing vmw-cli to fetch tkg cli from my VMware..."
    npm install vmw-cli --global && export PATH=$PATH:~/.npm-global/bin/
    
    echo "Use vmw cli to index the list..."
    vmw-cli list && vmw-cli index vmware-tanzu-kubernetes-grid
    
    cd /usr/bin
    
    for cli in `jq -r '.["TKG-100"].files | keys[]' /usr/lib/node_modules/vmw-cli/lib/fileIndex.json | egrep 'tkg-linux|clusterawsadm-linux'`; \
    do vmw-cli get $cli && gunzip $cli ; done
    
    mv clusterawsadm-linux-* clusterawsadm && chmod +x clusterawsadm
    mv tkg-linux-* tkg && chmod +x tkg
    
    echo "Installing Kubectl cli..."
    curl -L https://storage.googleapis.com/kubernetes-release/release/v1.17.4/bin/linux/amd64/kubectl -o kubectl && chmod +x kubectl
    kubectl version --client=true
    
    echo "Installing Kind cli..."
    curl -L "https://kind.sigs.k8s.io/dl/v0.7.0/kind-$(uname)-amd64" -o kind && chmod +x kind
}

function setup_tkg_bucket {
    
    if [[ -z "$(aws s3api list-buckets | jq -r ".Buckets[] | select(.Name==\"$TKG_BUCKET\") | .Name")" ]]; then
      echo "bucket does not exist, creating tkg management bucket"
      aws s3 mb "s3://${TKG_BUCKET}"
    else
      echo "bucket exists"
    fi
    
}

function export_env_variables {
    
    echo "Encoding AWS credemtials using clusteradm cli..."
    export AWS_B64ENCODED_CREDENTIALS=$(clusterawsadm alpha bootstrap encode-aws-credentials)
    
    echo "Fetching AMI details in environment variable..."
    export AWS_AMI_ID=$(cat tkg-ami.json | jq -r ".[] | select(.Region==\"$AWS_REGION\") | .AMI")
    
    export AWS_REGION=${AWS_REGION}
    export AWS_NODE_AZ="${AWS_REGION}a"
    export AWS_PUBLIC_NODE_CIDR="10.0.1.0/24"
    export AWS_PRIVATE_NODE_CIDR="10.0.0.0/24"
    export AWS_VPC_CIDR="10.0.0.0/16"
    export CLUSTER_CIDR="100.96.0.0/11"
    export AWS_SSH_KEY_NAME=${TKG_KEYPAIR}
    export CONTROL_PLANE_MACHINE_TYPE="t3.medium"
    export NODE_MACHINE_TYPE="t3.medium"
}

function setup_management_cluster {
    
    export_env_variables
    
    echo "Setup Kind cluster for TKG bootstrap..."
    kind create cluster --name "tkg-bootstrap" --wait 240s --config=./kind-config.yml
    
    echo "Fix kubeconfig to talk to local cluster..."
    sed -i -E -e 's/localhost|0\.0\.0\.0/docker/g' "$HOME/.kube/config"
    
    echo "Bootstrapping AWS cloudformation stack..."
    clusterawsadm alpha bootstrap create-stack
    
    echo "creating AWS key pair..."
    if [[ -z "$(aws ec2 describe-key-pairs | jq -r ".KeyPairs[] | select(.KeyName==\"$TKG_KEYPAIR\") | .KeyName")" ]]; then
      echo "Keypair does not exist, creating tkg management Keypair"
      aws ec2 create-key-pair --key-name $TKG_KEYPAIR --output json | jq .KeyMaterial -r > tkg-automation.pem
    else
      echo "Keypair exist"
    fi
    
    echo "Creating management cluster...."
    tkg init --infrastructure aws -p dev --name $TKG_MGMT_CLUSTER -e -v 6 
    
    cleanup_kind
    
    aws s3 sync $HOME/.tkg/ "s3://${TKG_BUCKET}"
    aws s3 cp $KUBECONFIG "s3://${TKG_BUCKET}/kube/"
    
    
}

function cleanup_management_cluster {
    
    export_env_variables
    
    echo "Copy kubeconfig from S3 bucket..."
    aws s3 cp "s3://${TKG_BUCKET}/kube/config" "$KUBECONFIG"
    
    mkdir -p $HOME/.tkg/
    
    echo "Copy tkg config from S3 bucket to local directory..."
    aws s3 cp "s3://${TKG_BUCKET}/config.yaml" $TKG_CONFIG
    # aws s3 sync "s3://${TKG_BUCKET}/providers" $HOME/.tkg/providers/
    
    echo "Setup Kind cluster for TKG bootstrap..."
    kind create cluster --name "tkg-bootstrap" --wait 240s --config=./kind-config.yml
    
    echo "Fix kubeconfig to talk to local cluster..."
    sed -i -E -e 's/localhost|0\.0\.0\.0/docker/g' "$HOME/.kube/config"
    
    echo "Cleanup TKG management cluster..."
    tkg delete management-cluster -y -e --config $TKG_CONFIG -v 10
    
    cleanup_kind
}

function cleanup_kind {
  kind delete cluster --name "tkg-bootstrap" && sleep 120
}

operation=$1

case $operation in
  cli)
    download_cli
    ;;
  tkg_bucket)
    setup_tkg_bucket
    ;;
  mgmt_cluster)
    setup_management_cluster
    ;;
  wc_cluster)
    setup_workload_cluster
    ;;
  mgmt_cleanup)
    cleanup_management_cluster
    ;;
  *)
    echo -e $"Usage: $0 {tkg_cli|mgmt_cluster|setup_workload_cluster}"
    exit 1
esac